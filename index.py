import time

from pyrogram import Client
from pyrogram.api import functions, types
from pyrogram.api.errors import (
    BadRequest, FloodWait, InternalServerError,
    SeeOther, Unauthorized, UnknownError
)

client = Client("support")
client.start()

client.stop()

# Now the "users" list contains all the members of the target chat