import time

from pyrogram import Client
from pyrogram.api import functions, types
from pyrogram.api.errors import (
    BadRequest, FloodWait, InternalServerError,
    SeeOther, Unauthorized, UnknownError
)

app = Client("support")

@app.on_message()
def my_handler(client, message):
    print(message)


app.start()
app.idle()

# Now the "users" list contains all the members of the target chat