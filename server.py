from main import app
import config

if __name__ == '__main__':
    app.run(debug=True, host=config.server['host'], port=config.server['port'])