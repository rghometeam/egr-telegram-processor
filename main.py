#! /usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import time

import config

from flask import Flask, request, render_template, redirect, url_for, jsonify

from string import ascii_lowercase
from random import randint
from pyrogram import Client
from pyrogram.api import functions, types
from pyrogram.api.errors import (
    BadRequest, FloodWait, InternalServerError,
    SeeOther, Unauthorized, UnknownError,
    MessageNotModified
)

from classes.JoinChatsStorage import JoinChatsStorage

from binance.client import Client as BinanceClient

app = Flask(__name__)
joinStorage = JoinChatsStorage('./joinchats/')

BINANCE_API_KEY = 'RUndoING9URx0vxfiicabTUPgqKGq4qWfKDk8YBYw8HKybUxz272Lv0eiYzH1vBm'
BINANCE_API_SECRET = 'XKoZPCjYEBOUj4za1z2D5doRB8zRof3a9UMauCUMPzpVFP6oLsqqTGK86ABIOKs0'

def getCoinPrice(coin):
    binance = BinanceClient(BINANCE_API_KEY, BINANCE_API_SECRET)

    priceData = [i for i in binance.get_all_tickers() if i['symbol'] == coin]

    return priceData[0]['price']

@app.route('/channel/invite', methods=['POST'])
def channel_invite():
    client = Client("support")
    client.start()

    requestData = request.json

    channelInviteHash = requestData['channel']

    user = requestData['user']

    data = client.send(
        functions.channels.InviteToChannel(
            channel=client.resolve_peer(channelInviteHash),
            users=[ client.resolve_peer(int(user['telegram_id'])) ]
        ),
    )

    client.stop()

    return json.dumps(user, default=lambda o: o.__dict__)

@app.route('/channel/ban', methods=['POST'])
def channel_ban():
    client = Client("support")
    client.start()

    requestData = request.json

    channelInviteHash = requestData['channel']

    user = requestData['user']
    
    data = client.send(
        functions.channels.EditBanned(
            channel=client.resolve_peer(channelInviteHash),
            user_id=client.resolve_peer(int(user['telegram_id'])),
            banned_rights=types.ChannelBannedRights(
                until_date=0,
                view_messages=False,
                send_messages=False,
                send_media=False,
                send_stickers=False,
                send_gifs=False,
                send_games=False,
                send_inline=False,
                embed_links=False
            )
        ),
    )

    client.stop()

    return json.dumps(user, default=lambda o: o.__dict__)


@app.route('/send-code', methods=['POST'])
def send_code():
    client = Client("support")
    client.start()

    requestData = request.json
    user = requestData['user']

    contacts = client.send(functions.contacts.GetContacts(hash=0))

    contactsCount = int(contacts.saved_count)

    imported = client.send(
        functions.contacts.ImportContacts(
            contacts=[
                types.InputPhoneContact(
                    client_id=0,
                    phone=str(user['phone']),
                    first_name='-'.join([x for x in ('user', str(user['id'])) if x]),
                    last_name=str(None)
                )
            ]
        )
    )


    for importedUser in imported.users:
        if (imported.imported[0].user_id == importedUser.id):
            if (importedUser.mutual_contact == False):
                return json.dumps({'error': 'not_mutual'})

    tg_user = imported.users[0]

    client.send(
        functions.messages.SendMessage(
            peer=client.resolve_peer(tg_user.id),
            message='Code is: {0}'.format(user['auth_code']),
            random_id=randint(0, 9999)
        )
    )

    client.stop()

    return json.dumps(imported, default=lambda o: o.__dict__)

@app.route('/users', methods=['POST'])
def parse_users():
        client = Client("support")
        client.start()

        users = {}  # List that will contain all the users of the target chat
        limit = 200  # Amount of users to retrieve for each API call
        # offset = 0  # Offset starts at 0
        ids = []

        update = request.json

        if 'joinchat/' in update['group']:
            hash = update['group'].split('/')[4]

            try:
                join = client.send(
                    functions.messages.ImportChatInvite(
                        hash=hash,
                    )
                )

                joinStorage.set(hash, join.chats[0].id)

                update['group'] = int(join.chats[0].id)
            except UnknownError as e:
                if (e.x.error_message == 'USER_ALREADY_PARTICIPANT'):
                    update['group'] = int(joinStorage.get(hash))

        queries = [""] + [str(i) for i in range(10)] + list(ascii_lowercase)

        for q in queries:
            print("Searching for '{}'".format(q))

            offset = 0  # Offset starts at 0

            while True:
                try:
                    participants = client.send(
                        functions.channels.GetParticipants(
                            channel=client.resolve_peer(update['group']),
                            filter=types.ChannelParticipantsSearch(q),  # Filter by empty string (search for all)
                            offset=offset,
                            limit=limit,
                            hash=0
                        )
                    )
                except FloodWait as e:
                    # Very large channels will trigger FloodWait.
                    # When happens, wait X seconds before continuing
                    time.sleep(e.x)
                    continue

                if not participants.participants:
                    break  # No more participants left

                users.update({i.id: i for i in participants.users})

                offset += len(participants.participants)

        # print(users)
        for id in users:
            user = users[id]
            
            if (update['option'] == 1):
                ids.append([user.id])

            elif (update['option'] == 2 and user.username is not None):
                ids.append([user.username])

            elif (update['option'] == 3):
                ids.append([user.id, user.first_name, user.last_name])

            elif (update['option'] == 4):
                ids.append([user.id, user.username])

        client.stop()

        return jsonify(ids)

@app.route('/signals/reply', methods=['POST'])
def signals_reply():
    client = Client("support")
    client.start()

    channelInviteHash = 'https://t.me/joinchat/AAAAAEy8zCITECaPyi7-dA'

    requestData = request.json

    while True:
        try :
            data = client.send(
                functions.messages.SendMessage(
                    peer=client.resolve_peer(channelInviteHash),
                    message=requestData['message'],
                    random_id=randint(0, 9999),
                    reply_to_msg_id=requestData['reply_id']
                ),
            )
        except FloodWait as e:
            # Very large channels will trigger FloodWait.
            # When happens, wait X seconds before continuing
            time.sleep(e.x)
            continue
        break

    client.stop()

    return json.dumps(data, default=lambda o: o.__dict__)

@app.route('/signals/publish', methods=['POST'])
def sinals_publish():
    client = Client("support")
    client.start()

    channelInviteHash = 'https://t.me/joinchat/AAAAAEy8zCITECaPyi7-dA'

    requestData = request.json

    while True:
        try :
            data = client.send_message(
                chat_id=channelInviteHash,
                text=requestData['message'].replace("{{cp}}", getCoinPrice(requestData['pair']))
            )
        except FloodWait as e:
            # Very large channels will trigger FloodWait.
            # When happens, wait X seconds before continuing
            time.sleep(e.x)
            continue
        break

    client.stop()

    return json.dumps({'message_id': data.message_id})

@app.route('/log/send', methods=['POST'])
def logs_publish():
    client = Client("support")
    client.start()

    requestData = request.json

    channelSupportLogs = requestData['channel']

    while True:
        try :
            data = client.send_message(
                chat_id=channelSupportLogs,
                text=requestData['message']
            )
        except FloodWait as e:
            # Very large channels will trigger FloodWait.
            # When happens, wait X seconds before continuing
            time.sleep(e.x)
            continue
        break

    client.stop()

    return json.dumps({'message_id': data.message_id})

@app.route('/signals/edit', methods=['POST'])
def sinals_edit():
    client = Client("support")
    client.start()

    channelInviteHash = 'https://t.me/joinchat/AAAAAEy8zCITECaPyi7-dA'

    requestData = request.json

    while True:
        try :
            data = client.edit_message_text(
                chat_id=channelInviteHash,
                message_id=requestData['message_id'],
                text=requestData['message'].replace("{{cp}}", getCoinPrice(requestData['pair']))
            )
        except FloodWait as e:
            time.sleep(e.x)
            continue
        except UnknownError as e:
            if (e.x.error_message == 'MESSAGE_EDIT_TIME_EXPIRED'):
                break
        except MessageNotModified as e:
            break
        break

    client.stop()

    return json.dumps({'message_id': True})
