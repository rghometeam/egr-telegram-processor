
class JoinChatsStorage(object):

    def __init__(self, path):
        self.path = path

    def set(self, hash, id):
        file = open(self.path + hash, 'w')
        file.write(str(id))
        file.close()

    def get(self, hash):
        file = open(self.path + hash, 'r')
        return file.read() 